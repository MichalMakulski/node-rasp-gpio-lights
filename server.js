const http = require('http');
const fs = require('fs');
const URL = require('url');
const querystring = require('querystring');
const toggleLight = require('./lightsHandler');
const port = process.env.PORT || 1337;

http.createServer((req, res) => {
  const url = req.url;
  
  if (url === '/') {
      res.writeHead(200, {'content-type': 'text/html'});
      fs.createReadStream(`${__dirname}/client/index.html`).pipe(res);
      return;
  }

  if (url.indexOf('public') !== -1) {
      const fileName = url.replace(/\/public\/(\w)/, '$1');
      fs.createReadStream(`${__dirname}/client/${fileName}`).pipe(res);
      return;
  }
  
  if (url.indexOf('toggleLight') !== -1) {
      const lightColor = querystring.parse(URL.parse(url).query).lightColor;
      toggleLight(lightColor, res);
      return;
  }
  
  
}).listen(port, () => console.log('LED server started...'));