;(function () {
  
  var yellowBtn = document.getElementById('yellow');
  var blueBtn = document.getElementById('blue');
  
  yellowBtn.addEventListener('click', toggleLight, false);
  blueBtn.addEventListener('click', toggleLight, false);
  
  function toggleLight (ev) {
    var target = ev.target;
    var lightColor = target.id;
    
    fetch('/toggleLight?lightColor=' + lightColor)
      .then(function(res) {
        return res.json();
      })
      .then(function(data) {
        var state = data.state;
        var color = data.light;
      
        if (state) {
          target.style.backgroundColor = color;
        }
        else {
          target.style.backgroundColor = '#fff';
        }
      });
  }
  
})();