const gpio = require('pi-gpio');
const setup = {
  blue: {
    pin: 36,
    state: 0
  },
  yellow: {
    pin: 40,
    state: 0
  }
}

function toggleLight(lightColor, res) {
  if (setup[lightColor].state) {
    gpio.write(setup[lightColor].pin, 0, () => {
      gpio.close(setup[lightColor].pin, () => {
        setup[lightColor].state = 0;
        sendInfoToClient(lightColor, res);
      });
    })
  }
  else {
    gpio.open(setup[lightColor].pin, 'output', (err) => {
      gpio.write(setup[lightColor].pin, 1, () => {
        setup[lightColor].state = 1;
        sendInfoToClient(lightColor, res);
      })
    });
  }
}

function sendInfoToClient (lightColor, res) {
  res.writeHead(200, {'content-type': 'application/json'});
  res.end(JSON.stringify({
    light: lightColor,
    state: setup[lightColor].state
  }));
}

module.exports = toggleLight;